
package com.tom1271.testmod.lib.load;

import net.minecraft.block.material.Material;

import com.tom1271.testmod.TestMod;
import com.tom1271.testmod.blocks.BlockEnderGlass;
import com.tom1271.testmod.blocks.BlockEnderOre;

public class LoadBlock {
    public LoadBlock() {

        // Here goes all of the Blocks to load them into Minecraft 

        TestMod.enderOre = new BlockEnderOre(TestMod.blockEnderOreID, Material.rock);
        TestMod.blockEnderGlass = new BlockEnderGlass(TestMod.blockEnderGlassID, Material.glass, true);

    }
}
