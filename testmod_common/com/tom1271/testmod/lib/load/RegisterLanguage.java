package com.tom1271.testmod.lib.load;

import com.tom1271.testmod.TestMod;
import com.tom1271.testmod.lib.Reference;

import cpw.mods.fml.common.registry.LanguageRegistry;

public class RegisterLanguage extends LanguageRegistry {

    public RegisterLanguage() {

        // Block Names
        LanguageRegistry.addName(TestMod.enderOre, "Ender Ore");
        LanguageRegistry.addName(TestMod.blockEnderGlass, "Ender Glass");

        // Item Names 
        LanguageRegistry.addName(TestMod.inertEnderPearl, "Inert Ender Pearl");

        LanguageRegistry.instance().addStringLocalization("itemGroup.tabsTM",
                "en_US", Reference.MOD_NAME);
    }

}
