package com.tom1271.testmod.core.proxy;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import cpw.mods.fml.common.network.IGuiHandler;

public class CommonProxy implements IGuiHandler {
    
    public void registerRenderInformation() {
        
        
    }

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world,
            int x, int y, int z) {
        
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world,
            int x, int y, int z) {
        
        return null;
    }
    
    
    // Registering Tile Entities
    public void registerTiles() {
        
    }
    
    //Registering Blocks
    public void registerBlocks() {
        
    }
    
    // Adding item's ingame names 
    public void addNames() {
        
    }
    
    // Adding item recipes
    public void addRecipes() {
        
    }

}
