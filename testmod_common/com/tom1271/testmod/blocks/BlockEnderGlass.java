package com.tom1271.testmod.blocks;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockBreakable;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;

import com.tom1271.testmod.TestMod;
import com.tom1271.testmod.lib.Textures;

public class BlockEnderGlass extends BlockBreakable {

    public BlockEnderGlass(int par1, Material par2Material, boolean par3) {
        super(par1, "glass", par2Material, par3);
        this.setUnlocalizedName("blockEnderGlass");
        this.setCreativeTab(TestMod.tabsTM);
        this.setLightOpacity(1);
        this.setStepSound(Block.soundGlassFootstep);
        this.setHardness(3F);
    }

    @Override
    public void registerIcons(IconRegister iconRegister) {
        this.blockIcon = iconRegister
                .registerIcon(Textures.BLOCK_TEXTURE_LOCATION
                        + this.getUnlocalizedName().substring(5));
    }

    public int quantityDropped(Random random) {
        return 1;
    }

    public boolean isOpaqueCube() {
        return false;
    }

    public boolean renderAsNormalBlock() {
        return false;
    }

}
