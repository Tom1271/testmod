/**
package com.tom1271.testmod.creativetab;

import net.minecraft.creativetab.CreativeTabs;

import com.tom1271.testmod.TestMod;

import cpw.mods.fml.relauncher.*;

/**
 * TestMod
 * 
 * TestMod Crative tab
 * 
 * @author Tom1271
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 *  
 */

/**
@SuppressWarnings("unused")
public class CreativeTabTM extends CreativeTabs {

    public CreativeTabTM(int position, String tabID) {
        super(position, tabID);
       
    }

    @SideOnly(Side.CLIENT)
    public int getTabIconItemIndex(){
        
        return 0;        
    }
    
    public String getTranslatedTabLabel(){
        
        return "Test Mod";
    }
}
 */