package com.tom1271.testmod.worldgen;

import java.util.Random;

import com.tom1271.testmod.TestMod;

import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import cpw.mods.fml.common.IWorldGenerator;

public class TMWorldGenerator implements IWorldGenerator {

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world,
            IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
        switch (world.provider.dimensionId) {
            case 1:
                generateEnd(world, random, chunkX * 16, chunkZ * 16);
            case 0:
                generateSurface(world, random, chunkX * 16, chunkZ * 16);
            case -1:
                ganerateNether(world, random, chunkX * 16, chunkZ * 16);
        }

    }

    private void ganerateNether(World world, Random random, int chunkX,
            int chunkZ) {

    }

    private void generateSurface(World world, Random random, int chunkX,
            int chunkZ) {
        
        // World generator Ender Ore
        for (int i = 0; i < 10; i++) {
            int coordX = random.nextInt(16) + chunkX;
            int coordZ = random.nextInt(16) + chunkZ;
            int coordY = random.nextInt(32);

            (new WorldGenMinable(TestMod.enderOre.blockID, 7)).generate(world,
                    random, coordX, coordY, coordZ);
            
            
        }

    }

    private void generateEnd(World world, Random random, int chunkX, int chunkZ) {

    }

}
