package com.tom1271.testmod;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;

import com.tom1271.testmod.core.handlers.ClientPacketHandler;
import com.tom1271.testmod.core.handlers.ServerPacketHandler;
import com.tom1271.testmod.lib.Reference;
import com.tom1271.testmod.lib.load.HarvestLevels;
import com.tom1271.testmod.lib.load.LoadBlock;
import com.tom1271.testmod.lib.load.LoadItem;
import com.tom1271.testmod.lib.load.RegisterGame;
import com.tom1271.testmod.lib.load.RegisterLanguage;
import com.tom1271.testmod.lib.load.TMRecipes;
import com.tom1271.testmod.worldgen.TMWorldGenerator;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkMod.SidedPacketHandler;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * TestMod
 * 
 * TestMod main class
 * 
 * @author Tom1271
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */

@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.VERSION)
@NetworkMod(clientSideRequired = true, serverSideRequired = false, clientPacketHandlerSpec = @SidedPacketHandler(channels = { "testMod" }, packetHandler = ClientPacketHandler.class), // For
// client-side
// packet
// handling
serverPacketHandlerSpec = @SidedPacketHandler(channels = {}, packetHandler = ServerPacketHandler.class))
// For server-side packet handling
public class TestMod {

    @Instance("TestMod")
    public static TestMod instance;

    public static Block enderOre;
    public static Block blockEnderGlass;

    public static Item inertEnderPearl;

    public static CreativeTabs tabsTM = new CreativeTabs("tabsTM") {
        public ItemStack getIconItemStack() {
            return new ItemStack(inertEnderPearl, 1, 0);
        }
    };

    public static int blockEnderOreID;
    public static int blockEnderGlassID;

    public static int itemInertEnderPearlID;

    @PreInit
    public void preInit(FMLPreInitializationEvent event) {
        Configuration config = new Configuration(
                event.getSuggestedConfigurationFile());

        config.load();

        blockEnderOreID = config.get("Block IDs", "Ender Ore ID", 3500).getInt();
        blockEnderGlassID = config.get("Block IDs", "Ender Glass ID", 3501).getInt();

        itemInertEnderPearlID = config.get("Item IDs", "Inert Ender Pearl ID", 3551)
                .getInt();

        config.save();
    }

    @Init
    public void load(FMLInitializationEvent event) {

        new LoadBlock();
        new LoadItem();
        new HarvestLevels();
        new RegisterLanguage();
        new RegisterGame();
        new TMRecipes();

        
        GameRegistry.registerWorldGenerator(new TMWorldGenerator());
    }

    @PostInit
    public void postInit(FMLPostInitializationEvent event) {

    }    
}
