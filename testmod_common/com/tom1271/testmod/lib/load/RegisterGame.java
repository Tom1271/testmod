package com.tom1271.testmod.lib.load;

import com.tom1271.testmod.TestMod;

import cpw.mods.fml.common.registry.GameRegistry;

public class RegisterGame {
    public RegisterGame() {

        GameRegistry.registerBlock(TestMod.enderOre, "TestMod"
                + TestMod.enderOre.getUnlocalizedName2());
        
        GameRegistry.registerBlock(TestMod.blockEnderGlass, "TestMod"
                + TestMod.blockEnderGlass.getUnlocalizedName2());

    }
}
