package com.tom1271.testmod.lib;

/**
 * TestMod
 * 
 * Textures
 * 
 * @author Tom1271
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 *  
 */

public class Textures {

    // Base file pathes
    public static final String ITEM_TEXTURE_LOCATION = "testmod:";
    public static final String BLOCK_TEXTURE_LOCATION = "testmod:";
    
    // Item/Block sprite sheets
    public static final String VANILLA_ITEM_TEXTURE_SHEET = "/gui/items.png";
    public static final String VANILLA_BLOCK_TEXTURE_SHEET = "/terrain.png";
}
