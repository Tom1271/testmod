package com.tom1271.testmod.lib;

/**
 * TestMod
 * 
 * Reference class
 * 
 * @author Tom1271
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 *  
 */

public class Reference {

      // General mod related constants 
    public static final String MOD_ID = "TestMod";
    public static final String MOD_NAME = "Test Mod";
    public static final String VERSION = "0.0.1";
    public static final String CHANNEL_NAME = MOD_ID;
    
}
