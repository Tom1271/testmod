package com.tom1271.testmod.items;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;

import com.tom1271.testmod.TestMod;
import com.tom1271.testmod.lib.Textures;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class ItemInertEnderPearl extends Item {

    public ItemInertEnderPearl(int id) {
        super(id);
        this.setCreativeTab(TestMod.tabsTM);
        this.setUnlocalizedName("inertEnderPearl");
        this.setMaxStackSize(16);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void updateIcons(IconRegister iconRegister) {
        iconIndex = iconRegister.registerIcon(Textures.ITEM_TEXTURE_LOCATION
                + this.getUnlocalizedName().substring(5));
    }

    
}
