package com.tom1271.testmod.lib.load;

import com.tom1271.testmod.TestMod;

import net.minecraftforge.common.MinecraftForge;

public class HarvestLevels {
    public HarvestLevels() {
        
        /*
         * Harvest levels: 0 = wood, 1 = stone 2 = Iron 3 = Diamond
         */

        MinecraftForge.setBlockHarvestLevel(TestMod.enderOre, TestMod.blockEnderOreID, "pickaxe", 3);
        MinecraftForge.setBlockHarvestLevel(TestMod.blockEnderGlass, TestMod.blockEnderGlassID, "pickaxe", 1);
        
    }
}
