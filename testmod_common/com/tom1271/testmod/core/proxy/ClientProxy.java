package com.tom1271.testmod.core.proxy;

import com.tom1271.testmod.core.proxy.CommonProxy;
import com.tom1271.testmod.lib.Textures;

import net.minecraftforge.client.MinecraftForgeClient;

/**
 * TestMod
 * 
 * TestMod Client proxy
 * 
 * @author Tom1271
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 *  
 */

public class ClientProxy extends CommonProxy{

    

    public void registerrenderInformation() {
        
        MinecraftForgeClient.preloadTexture(Textures.BLOCK_TEXTURE_LOCATION + "oreEnder.png");
        
    }
}
