package com.tom1271.testmod.blocks;

import java.util.ArrayList;
import java.util.Random;

import com.tom1271.testmod.TestMod;
import com.tom1271.testmod.items.ItemInertEnderPearl;
import com.tom1271.testmod.lib.Textures;

import cpw.mods.fml.common.registry.LanguageRegistry;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.client.MinecraftForgeClient;

@SuppressWarnings("unused")
public class BlockEnderOre extends Block {

    public BlockEnderOre(int id, Material material) {
        super(id, material);
        this.setUnlocalizedName("blockEnderOre");
        this.setHardness(3.0F);
        this.setStepSound(Block.soundStoneFootstep);
        this.setCreativeTab(TestMod.tabsTM);
        this.setResistance(10F);

    }

    @Override
    public void registerIcons(IconRegister iconRegister) {
        this.blockIcon = iconRegister
                .registerIcon(Textures.BLOCK_TEXTURE_LOCATION
                        + this.getUnlocalizedName().substring(5));
    }

    public int idDropped(int par1, Random rand, int par2) {
        return TestMod.inertEnderPearl.itemID;
    }

    public int quantityDropped(Random rand) {
        return rand.nextInt(4 - 1) + 1;
    }

    public void onBlockDestrouedByPlayer(World world, int x, int y, int z,
            int meta) {
        this.dropXpOnBlockBreak(world, x, y, z, 3);
    }
}
